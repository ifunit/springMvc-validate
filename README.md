#springMvc-validate
    <mvc:interceptors>
		<!-- 验证拦截器 -->
		<bean id="validatorInterceptor" class="com.ifunit.core.validator.interceptor.ValidatorInterceptor">
			<property name="validator">
				<bean class="com.ifunit.core.validator.DefaultValidator" />
			</property>
		</bean>
	</mvc:interceptors>
    
    Controlloer
    @Validations(

    requiredStringValidators = {
                                @RequiredStringValidator(field = "name", message = "姓名不能为空!", trim = true),
                                @RequiredStringValidator(field = "email", message = "邮箱不能为空!", trim = true),
                                @RequiredStringValidator(field = "mobile", message = "手机不能为空!", trim = true),
                                @RequiredStringValidator(field = "username", message = "用户名不能为空!", trim = true),
                                @RequiredStringValidator(field = "userType", message = "用户类型不能为空!", trim = true) },

    stringLengthValidators = { @StringLengthValidator(field = "username", message = "用户名只能在6-12位之间", minLength = "5", maxLength = "12") },

    emailValidators = { @EmailValidator(field = "email", message = "邮箱格式不正确!") })
    @RequestMapping(value = "/admin_save")
    public String save(HttpServletRequest request, ModelMap model,
                       @RequestParam(value = "rePassword") String rePassword, Admin admin) {

        if (admin.getId() == null && StringUtils.isBlank(admin.getPassword())) {
            FormUtils.addFormError(request, "password", "密码不能为空!");
        } else if (StringUtils.isNotBlank(admin.getPassword())
                   && !StringUtils.equals(admin.getPassword(), rePassword)) {
            FormUtils.addFormError(request, "rePassword", "两次密码不一致!");
        }

        if (FormUtils.hasErrors(request)) {
            return "admin/user_input";
        }
        userService.save(admin);

        return "redirect:admin_list.jhtml";
    }
    
    然后通过FormUtils.getFormErrors来显示错误
    
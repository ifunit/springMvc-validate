package com.ifunit.core.validator.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ifunit.core.message.MessageResolver;

public class FormUtils {

    /** 表单的错误 */
    public final static String FORM_ERROR = "formErros";

    /** 日志 */
    private final static Logger log = LoggerFactory.getLogger(FormUtils.class);

    /**
     * 设置表单的错误
     * 
     * @param request
     * @param map
     */
    public static void setFormErrors(HttpServletRequest request, Map<String, String> map) {

        if (map != null && !map.isEmpty()) {
            request.setAttribute(FORM_ERROR, map);
        }

    }

    /**
     * 添加表单的错误
     * 
     * @param request
     * @param field
     * @param message
     */
    public static void addFormError(HttpServletRequest request, String field, String message) {
        addFormError(request, field, null, message);
    }

    /**
     * 添加表单的错误
     * 
     * @param request
     * @param field
     * @param key 资源文件key
     * @param message 默认的message
     * @param args 格式化资源文件参数
     */
    public static void addFormError(HttpServletRequest request, String field, String key,
                                    String message, Object... args) {
        String msg = null;
        if (StringUtils.isNotBlank(key)) {
            msg = MessageResolver.getMessage(request, key, args);
        }
        if (StringUtils.isBlank(msg) && StringUtils.isBlank(message)) {
            log.warn("表单验证field:{}时根据key{}默认message{}获取的验证错误字符串为空", new String[] { field, key,
                                                                                  message });
            msg = "无对应的message";
        } else if (StringUtils.isBlank(msg)) {
            msg = message;
        }
        getFormErrors(request).put(field, msg);
    }

    /**
     * 获取表单的错误信息
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> getFormErrors(HttpServletRequest request) {
        Map<String, String> map = (Map<String, String>) request.getAttribute(FORM_ERROR);
        if (map == null) {
            map = new LinkedHashMap<String, String>();
        }

        for (Entry<String, String> entry : map.entrySet()) {
            log.trace("表单验证 field:{} error:{}", entry.getKey(), entry.getValue());
        }

        return map;
    }

    /**
     * 是否有表单错误
     * 
     * @param request
     * @return
     */
    public static boolean hasErrors(HttpServletRequest request) {
        return !getFormErrors(request).isEmpty();
    }

}
